﻿using System;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NSubstitute.ClearExtensions;
using NUnit.Framework;
using WebFitDiary.Models;
using WebFitDiary.Repositories.Interfaces;

namespace WebFitDiary.Tests
{
    [TestFixture, Category("Repository tests")]
    public class RepositoryTests
    {
        private IFitDiaryRepository _repository;
        private List<TrainingPlan> _trainingPlans;

        public RepositoryTests()
        {
            _repository = Substitute.For<IFitDiaryRepository>();
        }

        private Exercise _exercise = new Exercise
        {
            Id = 1,
            Name = "Deadlift",
            Reps = 3,
            Sets = 3,
            TrainingDay = 0
        };

        private TrainingPlan _trainingPlan = new TrainingPlan
        {
            Name = "plan",
            Exercises = new List<Exercise>()
        };

        private readonly TrainingPlan _anotherTrainingPlan = new TrainingPlan
        {
            Name = "planer"
        };

        [TearDown]
        public void CleanUp()
        {
            _repository.ClearSubstitute();
        }

        [SetUp]
        public void Init()
        {
            _trainingPlans = new List<TrainingPlan> { _trainingPlan, _anotherTrainingPlan };

            _repository.GetAllTrainingPlans()
                .Returns(_trainingPlans);

            _repository.GetTrainingPlanByName("plan")
                .Returns(_trainingPlan);
            _repository.GetTrainingPlanByName("planer")
                .Returns(_anotherTrainingPlan);
        }

        [Test]
        public void get_all_training_plans__returns_all_training_plans_that_exist_in_repository()
        {
            var expected = _trainingPlans;

            var result = _repository.GetAllTrainingPlans();

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void get_training_plan_by_name__returns_correct_training_plan()
        {
            Assert.AreEqual(_repository.GetTrainingPlanByName("plan"), _trainingPlan);
            Assert.AreNotEqual(_repository.GetTrainingPlanByName("plan"), _anotherTrainingPlan);
        }
    }
}