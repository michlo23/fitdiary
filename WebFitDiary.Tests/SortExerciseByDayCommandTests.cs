﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;

namespace WebFitDiary.Tests
{
    [TestFixture, Category("Sort exercises command tests")]
    public class SortExerciseByDayCommandTests
    {
        private TrainingPlan _initialTrainingPlan;
        private ISortExerciseByDay _sut = new SortExerciseByDayCommand();

        [SetUp]
        public void Init()
        {
            _initialTrainingPlan = new TrainingPlan
            {
                Exercises = new List<Exercise>
                {
                    new Exercise {TrainingDay = 6},
                    new Exercise {TrainingDay = 3},
                    new Exercise {TrainingDay = 2},
                    new Exercise {TrainingDay = 4},
                    new Exercise {TrainingDay = 5},
                    new Exercise {TrainingDay = 0},
                    new Exercise {TrainingDay = 1}
                }
            };
        }

        [Test]
        public void when__exercises_unsorted_and_sort_is_called__expected_exercise_sorted_by_day_increasing()
        {
            var expected = new TrainingPlan
            {
                Exercises = new List<Exercise>
                {
                    new Exercise { TrainingDay = 0},
                    new Exercise { TrainingDay = 1},
                    new Exercise { TrainingDay = 2},
                    new Exercise { TrainingDay = 3},
                    new Exercise { TrainingDay = 4},
                    new Exercise { TrainingDay = 5},
                    new Exercise {TrainingDay = 6}
                }
            };

            _sut.Sort(_initialTrainingPlan);

            var result = _sut.GetSortedExercises();

            for (int i = 0; i < expected.Exercises.Count; i++)
            {
                Assert.AreEqual(expected.Exercises.ToList()[i].TrainingDay,
                    result.Exercises.ToList()[i].TrainingDay);
            }
        }

        [Test]
        public void when__exercises_unsorted_and_doubled_values__expected_exercise_sorted_by_day_increasing()
        {
            _initialTrainingPlan.Exercises.Add(new Exercise{ TrainingDay= 0 });
            _initialTrainingPlan.Exercises.Add(new Exercise{ TrainingDay= 2 });
            _initialTrainingPlan.Exercises.Add(new Exercise{ TrainingDay= 3 });

            var expected = new TrainingPlan
            {
                Exercises = new List<Exercise>
                {
                    new Exercise {TrainingDay = 0},
                    new Exercise {TrainingDay = 0},
                    new Exercise {TrainingDay = 1},
                    new Exercise {TrainingDay = 2},
                    new Exercise {TrainingDay = 2},
                    new Exercise {TrainingDay = 3},
                    new Exercise {TrainingDay = 3},
                    new Exercise {TrainingDay = 4},
                    new Exercise {TrainingDay = 5},
                    new Exercise {TrainingDay = 6}
                }
            };

            _sut.Sort(_initialTrainingPlan);

            var result = _sut.GetSortedExercises();

            for (int i = 0; i < expected.Exercises.Count; i++)
            {
                Assert.AreEqual(expected.Exercises.ToList()[i].TrainingDay,
                    result.Exercises.ToList()[i].TrainingDay);
            }
        }
    }
}