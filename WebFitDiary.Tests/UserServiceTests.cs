﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using NSubstitute;
using NUnit.Framework;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;

namespace WebFitDiary.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        private IUserService _userService;
        private User _mockUser;
        private IdentityResult _identityResult;

        [SetUp]
        public void Init()
        {
            _userService = Substitute.For<IUserService>();
            _mockUser = Substitute.For<User>();
            _mockUser.UserName = "IAlreadyExist";
            _mockUser.Email = "mock@mock.email";
            _mockUser.PasswordHash = "hashhash";
            _identityResult = IdentityResult.Failed(new IdentityError()
            {
                Description = "User name 'IAlreadyExist' is already taken"
            });
            _userService
                .Register(_mockUser.UserName, _mockUser.Email, _mockUser.PasswordHash)
                .Returns(_identityResult);
        }

        [Test]
        public void when_user_exists_service_returns_invalid_result()
        {
            var result = _userService
                .Register(_mockUser.UserName, _mockUser.Email, _mockUser.PasswordHash).Result;

            Assert.IsFalse(result.Succeeded);
            Assert.IsNotEmpty(result.Errors);
            Assert.AreEqual("User name 'IAlreadyExist' is already taken", result.Errors.FirstOrDefault().Description);
        }

        [Test]
        public void when_user_not_exists_expected_successful_creation()
        {
            _userService
                .Register(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>())
                .Returns(IdentityResult.Success);

            var result = _userService.Register("", "", "").Result;

            Assert.IsTrue(result.Succeeded);
        }
    }
}