using Moq;
using NUnit.Framework;
using WebFitDiary.Models;

namespace Tests {
    [TestFixture]
    public class DataValidatorTests {
        private string _firstString;
        private string _secondString;

        [SetUp]
        public void Setup() {
            _firstString = "firstString";
            _secondString = "secondString";
        }

        [Test]
        public void IsStringChangedMethodSupposeReturnTrue() {
            var result = DataValidator.IsStringChanged(_firstString, _firstString);
            Assert.AreEqual(result, true);
        }

        [Test]
        public void IsStringChangedMethodSupposeReturnFalse() {
            var result = DataValidator.IsStringChanged(_firstString, _secondString);
            Assert.AreEqual(result, false);
        }

    }
}