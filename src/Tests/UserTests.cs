using System;
using System.Reflection;
using Moq;
using NUnit.Framework;
using WebFitDiary.Models;

namespace Tests {
    [TestFixture]
    public class UserTests {
        private User _user;

        [SetUp]
        public void Setup() {
            _user = new User("randomUser@wp.pl", "secret", "janek");
        }

        [Test]
        public void ChangeEmailShouldSucceed() {
            var expectedEmail = "randomUser2@gmail.com";
            _user.ChangeEmail(expectedEmail);
            Assert.AreEqual(_user.Email, expectedEmail);
        }

        [Test]
        public void ChangeEmailShouldFailed() {
            var exception = Assert.Throws<Exception>(() => _user.ChangeEmail(_user.Email));
            Assert.IsNotNull(exception);
            Assert.IsTrue(exception.Message.Contains("must be diffrent than old one"));
        }

        [Test]
        public void ChangePasswordShouldSucceed() {
            var expectedPassword = "topSecret";
            _user.ChangePassword(expectedPassword);
            var userPassword = typeof (User).GetProperty("Password",
                BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_user);
            Assert.AreEqual(userPassword, expectedPassword);
        }

        [Test]
        public void ChangePasswordShouldFail() {
            var expectedPassword = "secret";
            var exception = Assert.Throws<Exception>(() => _user.ChangePassword(expectedPassword));
            Assert.IsNotNull(exception);
            Assert.IsTrue(exception.Message.Contains("must be diffrent than old one"));
        }
    }
}