﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NUglify.Helpers;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;
using WebFitDiary.ViewModels;
using ILogger = Microsoft.Build.Framework.ILogger;

namespace WebFitDiary.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger<AccountController> _logger;

        public AccountController(
            IUserService userService,
            SignInManager<User> signInManager,
            ILogger<AccountController> logger)
        {
            _userService = userService;
            _signInManager = signInManager;
            _logger = logger;
        }

        [Route("register")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await _userService.Register(vm.UserName, vm.Email, vm.Password);

            if (result.Succeeded)
            {
                LoggerExtensions.LogInformation(_logger, 3, "User created a new account with password");

                return RedirectToAction("Login", "Auth");
            }
            result.Errors.ForEach(e => vm.ErrorMessage.Add(e.Description));
            return View(vm);
        }
    }
}
