﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebFitDiary.Models;
using WebFitDiary.Repositories.Interfaces;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Controllers.Api
{
    [Authorize]
    [Route("/api/trainingPlan/{planName}/exercises")]
    public class ExerciseController : Controller
    {
        private readonly IFitDiaryRepository _repository;
        private readonly ILogger<ExerciseController> _logger;

        public ExerciseController(IFitDiaryRepository repository, ILogger<ExerciseController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Get(string planName)
        {
            try
            {
                var trainingPlan = _repository.GetTrainingPlanByName(planName);
                return Ok(Mapper.Map<TrainingPlanViewModel>(trainingPlan));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong when getting training plan: {planName}. Ex: {ex}");
            }

            return BadRequest("Failed to get training plan");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(string planName, [FromBody] ExerciseViewModel exercise)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newExercise = Mapper.Map<Exercise>(exercise);

                    _repository.AddExercise(planName, newExercise);

                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"api/trainingPlan/{planName}/{exercise.Name}",
                            Mapper.Map<ExerciseViewModel>(newExercise));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong adding new exercise into database: {ex}");
            }
            return BadRequest("Failed to add exercise.");
        }


        [HttpDelete("")]
        public async Task<IActionResult> Delete(string planName, [FromBody] ExerciseViewModel exercise)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var exerciseToDelete = Mapper.Map<Exercise>(exercise);

                    _repository.DeleteExercise(planName, exerciseToDelete);

                    if (await _repository.SaveChangesAsync())
                    {
                        // TODO : What kind of result should be returned when deleting an entity from database?
                        return Accepted($"api/trainingPlan/{planName}/exercises");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong when deleting exercise from database: {ex}");
            }
            return BadRequest("Failed to delete exercise");
        }
    }
}