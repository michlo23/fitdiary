﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;
using WebFitDiary.Repositories.Interfaces;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Controllers.Api
{
    [Route("/api/trainingPlan/track/{planName}")]
    public class TrackController : Controller
    {

        private readonly IFitDiaryRepository _repository;
        private readonly ILogger<TrainingPlanController> _logger;
        private readonly ISortExerciseByDay _sorter;

        public TrackController(IFitDiaryRepository repository, ILogger<TrainingPlanController> logger, ISortExerciseByDay sorter)
        {
            _repository = repository;
            _logger = logger;
            _sorter = sorter;
        }

        [HttpGet("")]
        public IActionResult Get(string planName)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var trainingPlan = _repository.GetTrainingPlanByName(planName);
                    _sorter.Sort(trainingPlan);
                    trainingPlan = _sorter.GetSortedExercises();

                    return Ok(Mapper.Map<TrainingPlanViewModel>(trainingPlan));
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Something went wrong when getting training plan by name. Ex: {e}");
            }

            return BadRequest("Error occured.");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(string planName, 
            [FromBody] ExecutedExerciseTrackerViewModel submitedProgress)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var progressToAdd = Mapper.Map<ExecutedExerciseTracker>(submitedProgress);

                    _repository.AddProgress(planName, progressToAdd);

                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"Created /api/trainingPlan/track/{planName}",
                    Mapper.Map<ExecutedExerciseTrackerViewModel>(progressToAdd)); 
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Something went from updating current progress. Ex: {e}");
            }

            return BadRequest("Error occured.");
        }

        [HttpPut("")]
        public async Task<IActionResult> Put(string planName, [FromBody] ExecutedExerciseTracker submitedProgress)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var progressToUpdate = Mapper.Map<ExecutedExerciseTracker>(submitedProgress);

                    _repository.UpdateProgress(planName, progressToUpdate);

                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"Updated /api/trainingPlan/track/{planName}",
                            Mapper.Map<ExecutedExerciseTracker>(progressToUpdate));
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"Something went from updating current progress. Ex: {e}");
            }

            return BadRequest("Error occured while updating entry.");
        }
    }
}