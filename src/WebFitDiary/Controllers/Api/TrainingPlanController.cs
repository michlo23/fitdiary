﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebFitDiary.Models;
using WebFitDiary.Repositories.Interfaces;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Controllers.Api
{
    [Route("/api/trainingPlan")]
    [Authorize]
    public class TrainingPlanController : Controller
    {
        private readonly IFitDiaryRepository _repository;
        private readonly ILogger<TrainingPlanController> _logger;

        public TrainingPlanController(IFitDiaryRepository repository, ILogger<TrainingPlanController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            try
            {
                return Ok(Mapper.Map<IEnumerable<TrainingPlanViewModel>>(
                    _repository.GetAllTrainingPlansForUser(this.User.Identity.Name)));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Couldn't return Training plan repository: {ex}");

                return BadRequest("Error occured");
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] TrainingPlanViewModel trainingPlan)
        {
            if (ModelState.IsValid)
            {
                var newTrainingPlan = Mapper.Map<TrainingPlan>(trainingPlan);
                newTrainingPlan.UserName = this.User.Identity.Name;

                _repository.AddTrainingPlan(newTrainingPlan);

                if (await _repository.SaveChangesAsync())
                {
                    return Created($"api/trainingPlan/{trainingPlan.Name}",
                        Mapper.Map<TrainingPlanViewModel>(newTrainingPlan));
                }
            }
            return BadRequest("Failed to save data to the database");
        }

        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromBody] TrainingPlanViewModel trainingPlan)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var trainingPlanToDelete = Mapper.Map<TrainingPlan>(trainingPlan);

                    _repository.DeleteTrainingPlan(trainingPlanToDelete);

                    if (await _repository.SaveChangesAsync())
                    {
                        return Accepted($"api/trainingPlan/{trainingPlan.Name}",
                            Mapper.Map<TrainingPlanViewModel>(trainingPlanToDelete));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to delete trainingPlan {trainingPlan}, ex: {ex}");
            }

            return BadRequest("Failed to delete training plan.");
        }
    }


}