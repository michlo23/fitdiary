﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService _userService;

        public AuthController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("ProgressControl", "Home");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var signInResult = await _userService.Login(
                    vm.Username,
                    vm.Password);

            if (signInResult.Succeeded)
            {
                return RedirectToAction("ProgressControl", "Home");
            }

            ModelState.AddModelError("", "Username or password is invalid.");
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _userService.LogOut();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}