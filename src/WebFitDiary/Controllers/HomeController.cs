﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebFitDiary.Models.Database;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Controllers
{
    public class HomeController : Controller
    {
        private IConfigurationRoot _config;
        private FitDiaryContext _context;

        public HomeController(IConfigurationRoot config, FitDiaryContext context)
        {
            _config = config;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult ProgressControl()
        {
            var data = _context.Users.ToList();
            return View(data);
        }

        public IActionResult TrainingPlan()
        {
            return View();
        }
    }
}
