﻿using WebFitDiary.Models;

namespace WebFitDiary.Infrastructure
{
    public interface ISortExerciseByDay
    {
        void Sort(TrainingPlan plan);

        TrainingPlan GetSortedExercises();
    }
}