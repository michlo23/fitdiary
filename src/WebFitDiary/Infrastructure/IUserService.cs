﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebFitDiary.Models;

namespace WebFitDiary.Infrastructure
{
    public interface IUserService
    {
        Task<IdentityResult> Register(string userName, string email, string password);
        Task<SignInResult> Login(string userName, string password);
        Task LogOut();
    }
}