﻿using System;
using System.Linq;
using WebFitDiary.Models;

namespace WebFitDiary.Infrastructure
{
    public class SortExerciseByDayCommand : ISortExerciseByDay
    {
        private TrainingPlan _sortedTrainingPlan;

        public void Sort(TrainingPlan plan)
        {
          var sortedExercises = plan.Exercises.OrderBy(exercise => exercise.TrainingDay).ToList();
            _sortedTrainingPlan = plan;
            _sortedTrainingPlan.Exercises = sortedExercises;
        }

        public TrainingPlan GetSortedExercises()
        {
            if (_sortedTrainingPlan == null) throw new ArgumentNullException(nameof(_sortedTrainingPlan));
            return _sortedTrainingPlan;
        }
    }
}