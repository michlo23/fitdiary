﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebFitDiary.Models;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace WebFitDiary.Infrastructure
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserService(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> Register(string userName, string email, string password)
        {
            var user = new User { UserName = userName, Email = email };

            return await _userManager.CreateAsync(user, password);
        }

        public async Task<SignInResult> Login(string userName, string password)
        {
            return await _signInManager.PasswordSignInAsync(
                userName,
                password,
                isPersistent: true,
                lockoutOnFailure: false);
        }

        public async Task LogOut()
        {
            await _signInManager.SignOutAsync();
        }
    }
}