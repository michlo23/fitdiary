﻿using System;
using System.Collections.Generic;

namespace WebFitDiary.Models
{
    public class BodyMeansurment
    {
        public int Id { get; set; }
        public decimal Weight { get; set; }
        public DateTime Date { get; set; }
        public decimal Height { get; set; }
        public decimal BicepSize { get; set; }
        public decimal WaistCircumference { get; set; }
        public decimal ThighCircumferrence { get; set; }
    }
}