﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace WebFitDiary.Models.Database
{
    public class FitDiaryContext : IdentityDbContext<User>
    {
        private readonly IConfigurationRoot _config;
        private readonly DbContextOptions _dbContextOptions;

        public FitDiaryContext(IConfigurationRoot config, DbContextOptions dbContextOptions) 
            : base(dbContextOptions)
        {
            _config = config;
            _dbContextOptions = dbContextOptions;
        }

        public DbSet<Food> Foods { get; set; }
        public DbSet<BodyMeansurment> Meansurments { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<TrainingPlan> TrainingPlans { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_config["ConnectionStrings:FitDiaryConnectionString"]);
        }
    }
}