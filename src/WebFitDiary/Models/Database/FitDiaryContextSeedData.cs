﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace WebFitDiary.Models.Database
{
    public class FitDiaryContextSeedData
    {
        private readonly FitDiaryContext _context;
        private readonly UserManager<User> _userManager;

        public FitDiaryContextSeedData(FitDiaryContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureDataSeed()
        {
            if (_context.Users.Any())
            {
                return;
            }

            var userMichal = new User
            {
                Age = 23,
                Email = "michal@webfitdiary.com",
                FullName = "Michal Sekula",
                UserName = "MichalUser",

                BodyMeansurment = new BodyMeansurment
                {
                    BicepSize = 31,
                    Date = DateTime.Now,
                    Height = 179,
                    ThighCircumferrence = 123,
                    WaistCircumference = 100
                },

                Food = new Food
                {
                    Carbohydrates = 1212342141232,
                    Protein = 123421,
                    Fat = 999
                }
            };

            var userLukasz = new User
            {
                Age = 24,
                Email = "lukasz@webfitdiary.com",
                FullName = "Lukasz Reszke",
                UserName = "LukaszUser",

                BodyMeansurment = new BodyMeansurment
                {
                    BicepSize = 40,
                    Date = DateTime.Now,
                    Height = 189,
                    ThighCircumferrence = 65,
                    WaistCircumference = 80
                },

                Food = new Food
                {
                    Carbohydrates = 1212342141232,
                    Protein = 123421,
                    Fat = 999
                },

                TrainingPlan = new TrainingPlan
                {
                    Name = "HollyWood",
                    Exercises = new List<Exercise>
                    {
                        new Exercise {Name = "Deadlift", Reps = 5, Sets = 3}
                    },
                    NumberOfTrainingDays = 3,
                }
            };

            var testTreningPlan = new TrainingPlan
            {
                Name = "HollyWood",
                Exercises = new List<Exercise>
                {
                    new Exercise {Name = "Deadlift", Reps = 5, Sets = 3}
                },
                NumberOfTrainingDays = 3,
            };

            await _userManager.CreateAsync(userLukasz, "P@ssw0rd!");
            _context.Foods.AddRange(userLukasz.Food);
            _context.Meansurments.AddRange(userLukasz.BodyMeansurment);

            await _userManager.CreateAsync(userMichal, "P@ssw0rd!");
            _context.Foods.AddRange(userMichal.Food);
            _context.Meansurments.AddRange(userMichal.BodyMeansurment);

            _context.TrainingPlans.Add(testTreningPlan);
            _context.Exercises.AddRange(testTreningPlan.Exercises);
            await _context.SaveChangesAsync();
        }
    }
}