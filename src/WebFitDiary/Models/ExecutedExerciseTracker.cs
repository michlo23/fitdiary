﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebFitDiary.Models
{
    public class ExecutedExerciseTracker
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; } = DateTime.Now.ToFileTime();
        public int ExerciseId { get; set; }
        public int Reps { get; set; }
        public int Sets { get; set; }
        public int ExecutionDateTime { get; set; } = DateTime.Now.Day;
        public decimal Weight { get; set; }
    }
}