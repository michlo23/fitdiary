﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebFitDiary.Models
{
    public class Exercise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Reps { get; set; }
        public int Sets { get; set; }
        [Range(0, 6)]
        [Display(Name = "Day of Week")]
        public int TrainingDay { get; set; }
    }
}