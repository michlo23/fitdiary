﻿using System;

namespace WebFitDiary.Models
{
    public class Food
    {
        public int Id { get; set; }
        public double Protein { get; set; }
        public double Carbohydrates { get; set; }
        public double Fat { get; set; }
    }
}