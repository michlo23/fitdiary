﻿using System;
using System.Collections.Generic;

namespace WebFitDiary.Models
{
    public class TrainingPlan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public int NumberOfTrainingDays { get; set; }
        public int NumberOfExercises { get; set; }
        public ICollection<Exercise> Exercises { get; set; }
        public ICollection<ExecutedExerciseTracker> ExercisesTracker { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}