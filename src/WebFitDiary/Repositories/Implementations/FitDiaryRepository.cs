﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using WebFitDiary.Models;
using WebFitDiary.Models.Database;
using WebFitDiary.Repositories.Interfaces;

namespace WebFitDiary.Repositories.Implementations
{
    public class FitDiaryRepository : IFitDiaryRepository
    {
        private readonly FitDiaryContext _context;

        public FitDiaryRepository(FitDiaryContext context)
        {
            _context = context;
        }
        public IEnumerable<TrainingPlan> GetAllTrainingPlans()
        {
            return _context.TrainingPlans
                .Include(e => e.Exercises);
        }

        public IEnumerable<TrainingPlan> GetAllTrainingPlansForUser(string userName)
        {
            return _context.TrainingPlans
                .Where(u => u.UserName.Equals((string) userName))
                .Include(t => t.Exercises)
                .Include(t => t.ExercisesTracker);
        }

        public TrainingPlan GetTrainingPlanByName(string planName)
        {
            return _context.TrainingPlans
                .Include(t => t.Exercises)
                .Include(t => t.ExercisesTracker)
                .FirstOrDefault(t => t.Name == planName);
        }

        public void AddExercise(string planName, Exercise newExercise)
        {
            var plan = GetTrainingPlanByName(planName);

            if (plan != null)
            {
                plan.Exercises.Add(newExercise);
                _context.Exercises.Add(newExercise);
            }
        }

        public void AddTrainingPlan(TrainingPlan trainingPlan)
        {
            _context.TrainingPlans.Add(trainingPlan);
        }

        public void DeleteExercise(string planName, Exercise exerciseToDelete)
        {
            var plan = GetTrainingPlanByName(planName);

            if (plan != null)
            {
                var toDelete = plan.Exercises.FirstOrDefault(n => n.Name == exerciseToDelete.Name);
                plan.Exercises.Remove(toDelete);
                _context.Exercises.Remove(toDelete);
            }
        }

        public void DeleteTrainingPlan(TrainingPlan trainingPlan)
        {
            if (trainingPlan != null)
            {
                var tp = _context.TrainingPlans
                    .Include(t => t.Exercises)
                    .Include(t => t.ExercisesTracker)
                    .FirstOrDefault(t => t.Name.Equals(trainingPlan.Name));
                tp.Exercises.Clear();
                _context.TrainingPlans.Remove(tp);
            }
        }

        public void AddProgress(string planName, ExecutedExerciseTracker submitedProgress)
        {
            var plan = GetTrainingPlanByName(planName);

            if (plan != null)
            {
                plan.ExercisesTracker.Add(submitedProgress);
                _context.Add(submitedProgress);
            }
        }

        public void UpdateProgress(string planName, ExecutedExerciseTracker progressToUpdate)
        {
            var plan = GetTrainingPlanByName(planName);

            if (plan != null && plan.ExercisesTracker.Any(
                    ex => ex.ExecutionDateTime == progressToUpdate.ExecutionDateTime
                          && ex.ExerciseId == progressToUpdate.ExerciseId))
            {
                var repeatedEx = plan.ExercisesTracker.FirstOrDefault(
                    ex => ex.ExecutionDateTime == progressToUpdate.ExecutionDateTime
                          && ex.ExerciseId == progressToUpdate.ExerciseId);
                repeatedEx.Reps = progressToUpdate.Reps;
                repeatedEx.Sets = progressToUpdate.Sets;
                repeatedEx.Weight = progressToUpdate.Weight;
            }
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}