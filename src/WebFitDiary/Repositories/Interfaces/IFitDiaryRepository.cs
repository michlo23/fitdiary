﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebFitDiary.Models;
using WebFitDiary.ViewModels;

namespace WebFitDiary.Repositories.Interfaces
{
    public interface IFitDiaryRepository
    {
        IEnumerable<TrainingPlan> GetAllTrainingPlans();
        IEnumerable<TrainingPlan> GetAllTrainingPlansForUser(string userName);

        TrainingPlan GetTrainingPlanByName(string planName);

        void AddExercise(string planName, Exercise newExercise);
        void AddTrainingPlan(TrainingPlan trainingPlan);
        void AddProgress(string planName, ExecutedExerciseTracker submitedProgress);

        void DeleteExercise(string planName, Exercise exerciseToDelete);
        void DeleteTrainingPlan(TrainingPlan trainingPlan);

        void UpdateProgress(string planName, ExecutedExerciseTracker progressToUpdate);

        Task<bool> SaveChangesAsync();
    }
}