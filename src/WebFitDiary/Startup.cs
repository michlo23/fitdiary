﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using WebFitDiary.Infrastructure;
using WebFitDiary.Models;
using WebFitDiary.Models.Database;
using WebFitDiary.Repositories.Implementations;
using WebFitDiary.Repositories.Interfaces;
using WebFitDiary.ViewModels;

namespace WebFitDiary {
    public class Startup {
        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional : true, reloadOnChange : true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional : true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment()) {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode : true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddSingleton(Configuration);

            services.AddDbContext<FitDiaryContext>();

            services.AddTransient<FitDiaryContextSeedData>();

            services.AddScoped<IFitDiaryRepository, FitDiaryRepository>();
            services.AddScoped<ISortExerciseByDay, SortExerciseByDayCommand>();
            services.AddScoped<IUserService, UserService>();

            services.AddIdentity<User, IdentityRole>(config =>
                {
                    config.User.RequireUniqueEmail = true;
                    config.Password.RequiredLength = 8;
                    config.Cookies.ApplicationCookie.LoginPath = "/Auth/Login";
                })
                .AddEntityFrameworkStores<FitDiaryContext>();

            services.AddMvc()
                .AddJsonOptions(config =>
                config.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            FitDiaryContextSeedData dbSeeder)
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<TrainingPlanViewModel, TrainingPlan>().ReverseMap();
                config.CreateMap<ExerciseViewModel, Exercise>().ReverseMap();
                config.CreateMap<ExecutedExerciseTrackerViewModel, ExecutedExerciseTracker>().ReverseMap();
            });

            app.UseIdentity();

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug(LogLevel.Information);

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            } else {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            dbSeeder.EnsureDataSeed().Wait();
        }
    }
}