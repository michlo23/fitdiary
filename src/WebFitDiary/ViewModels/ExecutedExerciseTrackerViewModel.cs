﻿using System;

namespace WebFitDiary.ViewModels
{
    public class ExecutedExerciseTrackerViewModel
    {
        public int ExerciseId { get; set; }
        public int Reps { get; set; }
        public int Sets { get; set; }
        public int ExecutionDateTime { get; set; }
        public decimal Weight { get; set; }
    }
}