﻿using Microsoft.Build.Framework;
using WebFitDiary.Models;

namespace WebFitDiary.ViewModels
{
    public class ExerciseViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Reps { get; set; }
        public int Sets { get; set; }
        public int TrainingDay { get; set; }
    }
}