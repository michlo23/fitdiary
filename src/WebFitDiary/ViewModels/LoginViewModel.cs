﻿using System.ComponentModel.DataAnnotations;

namespace WebFitDiary.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}