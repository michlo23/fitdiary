﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebFitDiary.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [DisplayName("Email")]
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 4, ErrorMessage = "Password must contain between 4-100 characters.")]
        [DisplayName("Password")]
        public string Password { get; set; }

        public string PasswordHash { get; set; }

        public List<string> ErrorMessage { get; set; } = new List<string>();
    }
}