﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebFitDiary.Models;

namespace WebFitDiary.ViewModels
{
    public class TrainingPlanViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int NumberOfExercises { get; set; }
        public int NumberOfTrainingDays { get; set; }
        public ICollection<Exercise> Exercises { get; set; }
        public ICollection<ExecutedExerciseTracker> ExercisesTracker { get; set; }
        public DateTime DateCreated = DateTime.Now;
    }
}