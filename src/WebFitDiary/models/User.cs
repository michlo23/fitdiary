using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace WebFitDiary.Models
{
    public class User : IdentityUser
    {
        public int Age { get; set; }
        public string FullName { get; set; }
        public BodyMeansurment BodyMeansurment;
        public Food Food;
        public TrainingPlan TrainingPlan;
    }
}