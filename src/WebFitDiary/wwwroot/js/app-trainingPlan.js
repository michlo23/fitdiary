﻿(function () {
    "use strict";

    angular.module("app-trainingPlan", ["ngRoute"])
        .config(function ($routeProvider, $locationProvider) {

            $locationProvider.hashPrefix('');

            $routeProvider.when("/",
                {
                    controller: "trainingPlanController",
                    controllerAs: "vm",
                    templateUrl: "/views/trainingPlanView.html"
                });

            $routeProvider.when("/track/:trainingPlanName",
                {
                    controller: "trackController",
                    controllerAs: "vm",
                    templateUrl: "/views/trackView.html"
                });

            $routeProvider.when("/edit/:trainingPlanName",
                {
                    controller: "trainingPlanEditorController",
                    controllerAs: "vm",
                    templateUrl: "/views/trainingPlanEditorView.html"
                });

            $routeProvider.otherwise({ redirectTo: "/" });
        });

})();