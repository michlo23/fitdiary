﻿// Write your Javascript code.
(function () {
    var $sidebarWrapperAndHeader = $("#sidebar, #wrapper, #header");
    var $icon = $("#sidebarToggle i.fa");

    $("#sidebarToggle").on("click", function () {
        $sidebarWrapperAndHeader.toggleClass("hide-sidebar");
        if ($sidebarWrapperAndHeader.hasClass("hide-sidebar")) {
            $icon.removeClass("fa-angle-left");
            $icon.addClass("fa-angle-right");
        } else {
            $icon.addClass("fa-angle-left");
            $icon.removeClass("fa-angle-right");
        }
    });
})();