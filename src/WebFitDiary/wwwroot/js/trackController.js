﻿(function () {
    "use-strict";

    angular.module("app-trainingPlan")
        .controller("trackController", trackController).filter('groupBy', function ($timeout) {
            return function (data, key) {
                if (!key) return data;
                var outputPropertyName = '__groupBy__' + key;
                if (!data[outputPropertyName]) {
                    var result = {};
                    for (var i = 0; i < data.length; i++) {
                        if (!result[data[i][key]])
                            result[data[i][key]] = [];
                        result[data[i][key]].push(data[i]);
                    }
                    Object.defineProperty(data, outputPropertyName, { enumerable: false, configurable: true, writable: false, value: result });
                    $timeout(function () { delete data[outputPropertyName]; }, 0, false);
                }
                return data[outputPropertyName];
            };
        });

    function trackController($scope, $routeParams, $http) {

        var vm = this;
        vm.isBusy = true;
        vm.errorMessage = "";
        vm.name = $routeParams.trainingPlanName;
        vm.trainingPlan = [];
        vm.enabledEdit = [];
        vm.selectedExercise = {};
        vm.progress = {};

        var url = "/api/trainingPlan/track/" + vm.name;

        $scope.dayOfWeek = function (dayIndex) {
            return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"][dayIndex];
        };


        Date.prototype.getCurrentWeekNumber = function () {
            var onejan = new Date(this.getFullYear(), 0, 1);
            var today = new Date(this.getFullYear(), this.getMonth(), this.getDate());
            var dayOfYear = ((today - onejan + 1) / 86400000);
            return Math.ceil(dayOfYear / 7);
        };

        var pageIndex = new Date().getCurrentWeekNumber();

        var loadTrainingPlan = function () {
            $http.get(url)
                .then(function (response) {
                    angular.copy(response.data, vm.trainingPlan);
                },
                function (error) {
                    vm.errorMessage = "Failed to load exercises" + error;
                })
                .finally(function () {
                    vm.isBusy = false;
                });
        };

        vm.submitProgress = function (exercise, index) {
            vm.progress[index].exerciseId = exercise.id;
            vm.progress[index].executionDateTime = pageIndex;
            $http.post(url, vm.progress[index])
                .then(function (response) {
                    vm.trainingPlan.push(response.data);
                    loadTrainingPlan();
                },
                function () {
                    vm.errorMessage = "Failed to submit progress" + error;
                });
        };

        vm.updateProgress = function (exercise, index) {
            vm.enabledEdit[index] = false;
            vm.progress[index].exerciseId = exercise.id;
            vm.progress[index].executionDateTime = pageIndex;
            $http.put(url, vm.progress[index])
                .then(function () {
                    loadTrainingPlan();
                },
                function () {
                    vm.errorMessage = "Failed to update progress" + error;
                });

        }


        vm.editProgress = function (index) {
            vm.enabledEdit[index] = true;
        }

        $scope.goToPreviousWeek = function () {
            if (pageIndex === 0) {
                pageIndex = 0;
                return;
            }
            pageIndex -= 1;
        };

        $scope.goToNextWeek = function () {
            pageIndex += 1;
        };

        $scope.getPageIndex = function () {
            return pageIndex;
        }

        $scope.findProgressForExercise = function (exercise) {
            vm.selectedTracker = [];

            for (var i = 0; i < vm.trainingPlan.exercisesTracker.length; i++) {
                if (vm.trainingPlan.exercisesTracker[i].exerciseId === exercise.id
                    && vm.trainingPlan.exercisesTracker[i].executionDateTime === pageIndex) {
                    vm.selectedTracker = vm.trainingPlan.exercisesTracker[i];
                    return true;
                }
            }
            return false;
        };

        $scope.totalCount = 0;
        $scope.countInit = function () {
            return $scope.totalCount++;
        };

        loadTrainingPlan();

    }
})();
