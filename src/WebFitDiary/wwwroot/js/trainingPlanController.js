﻿(function () {
    angular.module("app-trainingPlan")
        .controller("trainingPlanController", trainingPlanController);

    function trainingPlanController($http) {
        var url = "/api/trainingPlan";

        var vm = this;
        vm.trainingPlans = [];


        vm.newTrainingPlan = {};

        vm.errorMessage = "";

        vm.isBusy = true;

        var loadPlans = function() {
            $http.get(url)
                .then(function(response) {
                        angular.copy(response.data, vm.trainingPlans);
                    },
                    function(error) {
                        vm.errorMessage = "Failed to load data: " + error;
                    })
                .finally(function() {
                    vm.isBusy = false;
                });
        };

        loadPlans();


        vm.addNewTrainingPlan = function() {

            vm.isBusy = true;
            vm.errorMessage = "";

            $http.post(url, vm.newTrainingPlan)
                .then(function(response) {
                        vm.trainingPlans.push(response.data);
                        vm.newTrainingPlan = {};
                    },
                    function() {
                        vm.errorMessage = "Failed to save new training plan";
                    })
                .finally(function() {
                    vm.isBusy = false;
                });
        };

        vm.deleteTrainingPlan = function(plan) {
            vm.errorMessage = "";

            console.log("Trying to delete: " + plan);

            return $http({
                    method: "DELETE",
                    url: url,
                    headers: {
                        'Content-Type': "application/json"
                    },
                    data: plan
                }).then(function() {
                    loadPlans();
                }),
                function(error) {
                    // failure

                    vm.errorMessage = "Failed to delete training plan: " + plan.name;
                    console.log("Failed to delete training plan " + error);

                };
        };
    }
})();