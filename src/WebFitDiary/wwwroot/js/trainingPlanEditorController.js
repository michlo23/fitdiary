﻿(function () {
    "use-strict";

    angular.module("app-trainingPlan")
        .controller("trainingPlanEditorController", trainingPlanEditorController);

    function trainingPlanEditorController($routeParams, $http) {
        var vm = this;
        vm.name = $routeParams.trainingPlanName;
        vm.trainingPlan = [];
        vm.exercises = [];
        vm.errorMessage = "";
        vm.isBusy = true;
        vm.newExercise = {};
        var url = "/api/trainingPlan/" + vm.name + "/exercises";

        var loadTrainingPlan = function() {
            $http.get(url)
                .then(function(response) {
                        angular.copy(response.data, vm.trainingPlan);
                    },
                    function(error) {
                        vm.errorMessage = "Failed to load training plan" + error;
                    })
                .finally(function() {
                    vm.isBusy = false;
                });
        };


        loadTrainingPlan();

        vm.addNewExercise = function () {
            vm.isBusy = true;
            vm.errorMessage = "";

            $http.post(url, vm.newExercise)
                .then(function (response) {
                    // success
                    vm.exercises.push(response.data);
                    vm.newExercise = {};
                    loadTrainingPlan();
                }, function (error) {
                    // failure
                    vm.errorMessage = "Failed to add new exercise" + error;
                })
                .finally(function () {
                    vm.isBusy = false;
                });
        };

        vm.deleteExercise = function(exercise) {
            vm.isBusy = true;
            vm.errorMessage = "";

            console.log("Delete request sent...");
            console.log(exercise);

            return $http({
                    method: "DELETE",
                    url: url,
                    headers: {
                        'Content-Type': "application/json"
                    },
                    data: exercise
                }).then(function() {
                    loadTrainingPlan();
                }),
                function(error) {
                    // failure
                    vm.errorMessage = "Failed to delete " + exercise.name + "exercise";
                    console.log("Failed to delete object, error: ", error);
                };
        };
    }
})();